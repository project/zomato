README.txt for Zomato module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------

This module focuses on the integration of Zomato's Public API with Drupal.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

RECOMMENDED MODULES
-------------------

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

INSTALLATION
------------

 - Install the rtub module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
------------

* Configure the user permissions in Administration » People » Permissions:

This module procides following permission:
 - Administer Zomato API Configuration

* Configure the Zomato API Settings in /admin/config/zomato/adminsettings

This mpdules provides configuration form, where user store the zomato API key
and zomato API base url.

This module provides service, using this service any module will able to
access Zomato API and get response in JSON format.

Example :

$service = \Drupal::service('zomato.zomato_request');
$param = array();
$response = $service->request('GET', 'categories', $param));
