<?php

namespace Drupal\zomato\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Cnfiguration Form for Zomato Module.
 *
 * @internal
 */
class ZomatoConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'zomato.configsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zomato_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('zomato.configsettings');

    $form['zomato'] = [
      '#type' => 'details',
      '#title' => $this->t('Zomato API settings'),
      '#open' => TRUE,
    ];
    $form['zomato']['zomato_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zomato API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('zomato_api_key'),
    ];
    $form['zomato']['zomato_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zomato Base URL'),
      '#required' => TRUE,
      '#default_value' => $config->get('zomato_base_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('zomato.configsettings')
      ->set('zomato_api_key', $form_state->getValue('zomato_api_key'))
      ->set('zomato_base_url', $form_state->getValue('zomato_base_url'))
      ->save();
  }

}
