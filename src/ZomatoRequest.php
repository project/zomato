<?php

namespace Drupal\zomato;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for nodes of different types.
 */
class ZomatoRequest implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Constructs a DefaultFetcher object.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   A Guzzle client object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   */
  public function __construct(ClientFactory $http_client_factory, ConfigFactoryInterface $config_factory) {
    $this->httpClientFactory = $http_client_factory;
    $this->config = $config_factory->get('zomato.configsettings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('http_client_factory')
    );
  }

  /**
   * Zomato API Call.
   *
   * @param string $method
   *   GET OR POST.
   * @param string $call
   *   Pass Zomato API method name like categories, cities.
   * @param array $options
   *   (optional) Request Input Parameter.
   *
   * @return json
   *   Reurn data in JSON Format.
   */
  public function request($method, $call, array $options = []) {

    $api_url = $this->config->get('zomato_base_url');
    $api_key = $this->config->get('zomato_api_key');

    $client = $this->httpClientFactory->fromOptions([
      'base_uri' => $api_url,
    ]);

    $param = [];
    $param['headers'] = ['X-Zomato-API-Key' => $api_key];

    if (isset($options)) {
      $param['query'] = $options;
    }

    $response = $client->request($method, $call, $param);

    return Json::decode($response->getBody());
  }

}
